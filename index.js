var Tracer = require("./tracer");
var express = require('express');
var bunyan = require('bunyan');
//var log = bunyan.createLogger({
//  name: 'myapp',
//  streams: [
//    {
//      level: 'info',
//      path: '/home/jclegras/Documents/logstash/log/dummy.log'           // log INFO and above to stdout
//    }
//  ]
//});

var log = bunyan.createLogger({name: 'myapp'});

var app = express();

const tracer = new Tracer();
tracer.start();

//tracer.on(Tracer.STATS_EVENT, (info) => log.info(info));

//tracer.on(Tracer.GC_EVENT, (info) => log.info(info));

tracer.on(Tracer.MEM_EVENT, (info) => log.info(info));

app.get('/', function (req, res) {
    for (var i=0; i<1000; i++) {
        app.on('request', function leakyfunc() {});
    }
    res.send('Hello World!');
});


app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});


