'use strict';

const EventEmitter = require('events').EventEmitter;

const profiler = require('gc-profiler');
const reInterval = require('reinterval');
const memwatch = require('memwatch-next');

const STATS_EVENT = 'stats',
      GC_EVENT    = 'gc',
      MEM_EVENT   = 'mem';


class Tracer extends EventEmitter {
    constructor(optsArg) {
        super();
        const opts = optsArg || {};
        this._opts = {
            sampleInterval: opts.sampleInterval || 5,
        };
        this._intervalTime = this._secondsToMilliseconds(this._opts.sampleInterval);
        this._gcProfiler = profiler;
        this._memwatch = memwatch;
        this._emitInterval = reInterval(() => this._emitStats(), this._intervalTime);
        this._emitInterval.clear();
        this._stats = {
            timestamp: new Date(),
            process: {
                cpuUsage: process.cpuUsage(),
                memoryUsage: process.memoryUsage(),
                uptime: process.uptime(),
            },
        };
    }

    static get STATS_EVENT() {
        return STATS_EVENT;
    }

    static get GC_EVENT() {
        return GC_EVENT;
    }

    static get MEM_EVENT() {
        return MEM_EVENT;
    }

    start() {
        this._gcProfiler.on(GC_EVENT, (info) => this.emit(GC_EVENT, info));
        this._memwatch.on('stats', (info) => this.emit(MEM_EVENT, info));
        this._emitInterval.reschedule(this._intervalTime);
    }

    // update stats
    _generateStats() {
        this._stats.timestamp = new Date();
        this._stats.process.cpuUsage = process.cpuUsage();
        this._stats.process.memoryUsage = process.memoryUsage();
        this._stats.process.uptime = process.uptime();

        return this._stats;
    }

    _emitStats() {
        this.emit(STATS_EVENT, this._generateStats());
    }

    _secondsToMilliseconds(s) {
        return s * 1000;
    }
}

module.exports = Tracer;